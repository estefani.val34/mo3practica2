/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientmanagerap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.*;

/**
 *
 * @author alumne
 */
public class ClientManagerAp {

    ClientList myClientList;//dentro de myClientList esta my clients es la Store que tiene products : Store=ClientList, products=clients 

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        ClientManagerAp clientManager = new ClientManagerAp();
        clientManager.run();
    }

    /**
     * This method runs my application
     */
    private void run() {
        myClientList = new ClientList();
        Menu mainMenu = buildMainMenu();
        boolean exit = false;

        int optionSelected;
        loadClients();

        do {
            mainMenu.show();
            System.out.print("Choose an option: ");
            optionSelected = mainMenu.choose();

            switch (optionSelected) {
                case 0: //exit application.
                    exit = true;
                    break;
                case 1: //find a client by id.
                    findAClientById();
                    break;
                case 2: //list all clients.
                    listAllClients();
                    break;
                case 3: //add a new client.
                    addANewClient();
                    break;
                case 4: //modify a client.
                    modifyAClient();
                    break;
                case 5: //remove a client.
                    removeAClient();
                    break;
                default:
                    System.out.println("Invalid option");
                    break;
            }

        } while (!exit);
    }

    /**
     * buildMainMenu() Builds and returns the main menu of our application.
     */
    private static Menu buildMainMenu() {
        Menu mnu = new Menu("Client manager application");
        mnu.add(new Option("Exit"));
        mnu.add(new Option("Find a client by id"));
        mnu.add(new Option("List all clients"));
        mnu.add(new Option("Add a new client"));
        mnu.add(new Option("Modify a client"));
        mnu.add(new Option("Remove a client"));
        return mnu;
    }

    /**
     * findAClientById() Ask the user for a client id and searches for him in
     * the list.
     */
    private void findAClientById() {
        System.out.println("Searching client...");
        myClientList.findbyId(myClientList.getMyClients());
    }

    /**
     * listAllClients() Shows the list of clients.
     */
    private void listAllClients() {
        System.out.println("Listing all clients...");
        myClientList.listClients(myClientList.getMyClients());
    }

    /**
     * addANewClient() Adds a new client to the list. First, it asks for the new
     * client data to the user, creates a new client with that data, and add the
     * new client to the list.
     */
    private void addANewClient() {
        System.out.println("Adding a new client...");
        Client c = myClientList.dataclient(myClientList.getMyClients());
        myClientList.add(c);
    }

    /**
     * modifyAClient() Modifies a client. First, it asks the client id, then, it
     * look him up in the list. If found, it shows the actual client information
     * and ask for new data. Finally, it modifies the client. If not found, it
     * reports the error to the user.
     */
    private void modifyAClient() {
        System.out.println("Modifying a client...");
        myClientList.modifycli(myClientList.getMyClients());
    }

    /**
     * removeAClient() Removes a client. First, it asks the client id, then, it
     * look him up in the list. If found, it shows the actual client information
     * and ask for confirmation. After comfirmed, it removes the client. If not
     * found, it reports the error to the user.
     */
    private void removeAClient() {
        System.out.println("Removing a client...");
        myClientList.deletecli(myClientList.getMyClients());
       
    }

    /**
     * loadClients() Loads initial data into client list.
     */
    private void loadClients() {
        System.out.println("Loading client data into list of clients...");

        myClientList.add(new Client("001A", "Peter", "93001", "Addr01", 1001.0));
        myClientList.add(new Client("001B", "Paul", "93002", "Addr02", 1002.0));
        myClientList.add(new Client("001C", "Mary", "93003", "Addr03", 1003.0));
        myClientList.add(new Client("001D", "Bob", "93004", "Addr04", 1004.0));
        myClientList.add(new Client("001E", "Sophie", "93005", "Addr05", 1005.0));
        myClientList.add(new Client("001F", "Andrew", "93006", "Addr06", 1006.0));
        myClientList.add(new Client("001G", "Phil", "93007", "Addr07", 1007.0));
    }

}
