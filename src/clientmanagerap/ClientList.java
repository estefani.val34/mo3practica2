/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientmanagerap;

import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author tarda
 */
public class ClientList {

    private Set<Client> myClients;

    //constructor
    public ClientList() {
        myClients = new HashSet<>();
    }

    //getters +setters
    public Set<Client> getMyClients() {
        return myClients;
    }

    public void setMyClients(Set<Client> myClients) {
        this.myClients = myClients;
    }

    public void add(Client client) {
        myClients.add(client);
    }

    void listClients(Set<Client> myClients) {
        for (Client p : myClients) {
            System.out.println(p);
        }
    }

    public boolean findbyId(Set<Client> myClients) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter nif of client: ");
        String nif = sc.nextLine().toUpperCase();
        return findClient(myClients, nif);
    }

    public boolean findClient(Set<Client> myClients1, String nif) {
        boolean found = false;
        found = cliNoNull(myClients1, nif, found);
        if (found) {
            System.out.println("Exits a client with this nif!");
            System.out.println(findCl(myClients1, nif));
        }
        return found;
    }

    public boolean cliNoNull(Set<Client> myClients1, String nif, boolean found) {
        if (findCl(myClients1, nif).getNif() != null) {
            found = true;
        }
        return found;
    }

    public Client findCl(Set<Client> myClients1, String nif) {
        Client c = new Client();
        for (Client p : myClients1) {
            if (p.getNif().equals(nif)) {
                c = p;
            }
        }
        return c;
    }

    public Client dataclient(Set<Client> myClients) {
        Scanner sc = new Scanner(System.in);
        String nif = null;
        String name = null;
        String phone = null;
        String address = null;
        double amount = 0;
        boolean found = false;
        do {
            do {
                System.out.println("Enter nif:");
                nif = sc.nextLine().toUpperCase();
                found = findClient(myClients, nif);
            } while (found);
            System.out.println("Enter name:");
            name = sc.nextLine();
            System.out.println("Enter phone:");
            phone = sc.nextLine();
            System.out.println("Enter address:");
            address = sc.nextLine();
            System.out.println("Enter amount:");
            amount = sc.nextDouble();

        } while (found);

        Client c = new Client(nif, name, phone, address, amount);
        return c;
    }

    void modifycli(Set<Client> myClients) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter nif:");
        String nif = sc.nextLine().toUpperCase();
        boolean found = cliNoNull(myClients, nif, false);
        if (found) {
            Client cl = findCl(myClients, nif);
            System.out.println(cl);
            System.out.println("Enter new name:");
            String name = sc.nextLine();
            cl.setName(name);
            System.out.println("Enter new phone:");
            String phone = sc.nextLine();
            cl.setPhone(phone);
            System.out.println("Enter new address:");
            String address = sc.nextLine();
            cl.setAddress(address);
            System.out.println("Enter new amount:");
            double amount = sc.nextDouble();
            cl.setAmount(amount);
            System.out.println(cl);
        } else {
            System.out.println("No found this client");
        }

    }

    void deletecli(Set<Client> myClients) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter nif:");
        String nif = sc.nextLine().toUpperCase();
        boolean found = cliNoNull(myClients, nif, false);
        if (found) {
            Client cl = findCl(myClients, nif);
            myClients.remove(cl);
            System.out.println("Deleted");
        } else {
            System.out.println("No found this client");
        }
    }

}
